# Gitlab CI Image

* [Docker Image](https://hub.docker.com/r/petrusv/gitlab-ci-image)
* [Dockerfile](https://gitlab.com/micro-entreprise/gitlab-ci-image)


The intent of this Docker image is to use in gitlab ci project
it contains common tools for python/nodejs project pipline that
can build docker images.

Main applications pre-installed:


* Alpine

 * docker
 * git
 * less
 * curl
 * python3
 * npm/nodejs
 * postgresql-client
 * httpie

* Go app:

 * [envsubst](https://github.com/a8m/envsubst)

* Python libs:

 * invoke
 * pre-commit
 * black
 * flake8
 * docker-compose
 * git-aggregator
 * wheel
 * twine
 * s3cmd

## Usage

In gitlab-ci.yml

```
image: micro-entreprise/gitlab-ci-image:docker

...
```

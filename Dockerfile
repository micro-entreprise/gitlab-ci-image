FROM docker:26

RUN apk add --update \
    python3  \
    python3-dev \
    py3-virtualenv \
    docker-cli-compose \
    libxml2-dev \
    libxslt-dev \
    ca-certificates \
    groff \
    git \
    less \
    curl \
    openssh-client \
    bash \
    httpie \
    jq \
    postgresql-client \
    postgresql-libs \
    postgresql-dev \
    openssl-dev \
    build-base \
    libffi-dev \
    nodejs \
    npm \
    && rm -rf /var/cache/apk/* \
    && curl -L https://github.com/a8m/envsubst/releases/download/1.2.0/envsubst-Linux-x86_64 -o envsubst \
    && chmod +x envsubst \
    && mv envsubst /usr/local/bin
        
RUN python -m venv /opt/py3-venv \
    && . /opt/py3-venv/bin/activate \
    && pip --no-cache-dir install --upgrade pip wheel \
    && pip install black \
        distlib \
        flake8 \
        git-aggregator \
        invoke \
        pre-commit \
        s3cmd \
        twine

ENV PATH="${PATH}:/opt/py3-venv/bin"